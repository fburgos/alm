# ALM Applications

Docker-compose that runs several ALM applications

* Run all applications: `$ docker-compose up`
* Run just an application: `$ docker-compose up -d bitbucket`

In order to set base-urls right into setup applications is recommended to add containers internal dns as alias within `/etc/hosts/`

Example:

```
# localhost name resolution is handled within DNS itself.
	127.0.0.1       localhost postgres bitbucket jira teamcity
```